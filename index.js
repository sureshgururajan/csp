const { printCSPReport } = require('./util/printUtils.js')

const express = require('express')
const path = require('path')


const app = express()

app.use(express.urlencoded({ extended: true }))
app.use(express.json({"type": "application/json"}))
app.use(express.json({"type": "application/csp-report"}))


const port = 3000
const host = "0.0.0.0"


function setResponseHeaders(response) {
  response.append("Content-Security-Policy", "default-src 'self'; report-uri /csp-reports")
}


app.post('/csp-reports', (req, res) => {

  printCSPReport(req);

  res.status(204).end("");
});


app.get('/', (req, res) => {
  setResponseHeaders(res)
  res.sendFile(path.join(__dirname, '/index.html'));
});


app.listen(port, host, () => {
  console.log(`Example app listening at http://${host}:${port}`)
});
