const { log } = require('./logUtils.js')


function printCSPReport(request) {
  let body = JSON.stringify(request.body)
  log(`CSP Violation Reported`)
  log(`${body}`)
}


module.exports = { printCSPReport }
